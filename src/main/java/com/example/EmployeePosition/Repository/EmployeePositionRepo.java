package com.example.EmployeePosition.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.EmployeePosition.Model.EmployeePosition;

public interface EmployeePositionRepo extends JpaRepository<EmployeePosition, Long>{
	@Query("FROM EmployeePosition WHERE EmployeeId = ?1")
    List<EmployeePosition> FindByEmployeId(Long employeId);
	
	@Query("FROM EmployeePosition WHERE PositionId = ?1")
    List<EmployeePosition> FindByPositionId(Long positionId);
}
