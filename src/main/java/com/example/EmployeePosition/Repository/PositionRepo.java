package com.example.EmployeePosition.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeePosition.Model.Position;

public interface PositionRepo extends JpaRepository<Position, Long>{

}
