package com.example.EmployeePosition.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.EmployeePosition.Model.Biodata;

public interface BiodataRepo extends JpaRepository<Biodata, Long>{

}
