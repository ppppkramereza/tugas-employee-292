package com.example.EmployeePosition.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.EmployeePosition.Model.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long>{

	@Query("FROM Employee WHERE BiodataId = ?1")
    List<Employee> FindByBiodataId(Long biodataId);
}
