package com.example.EmployeePosition.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/employeeposition/")
public class EmployeePositionController {

	@GetMapping(value = "index")
	public ModelAndView index() {
		
		ModelAndView view = new ModelAndView("employeeposition/index");
		return view;
	}
}
