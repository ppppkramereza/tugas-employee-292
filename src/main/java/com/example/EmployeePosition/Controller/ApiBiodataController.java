package com.example.EmployeePosition.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.EmployeePosition.Model.Biodata;
import com.example.EmployeePosition.Repository.BiodataRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodataController {
    
    @Autowired
	private BiodataRepo biodataRepo;

    @GetMapping("/biodata")
	public ResponseEntity<List<Biodata>> GetAllBiodata(){
		try {
			List<Biodata> biodata = this.biodataRepo.findAll();
			return new ResponseEntity<>(biodata, HttpStatus.OK);

		}
		catch(Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
