package com.example.EmployeePosition.Controller;

import org.springframework.web.bind.annotation.*;

import com.example.EmployeePosition.Model.EmployeePosition;
import com.example.EmployeePosition.Repository.EmployeePositionRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiEmployeePositionController {
    @Autowired
	private EmployeePositionRepo employeepositionRepo;
	
	@GetMapping("/employeeposition")
	public ResponseEntity<List<EmployeePosition>> GetAllEmployeePosition(){
		try {
			List<EmployeePosition> employeeposition = this.employeepositionRepo.findAll();
			return new ResponseEntity<>(employeeposition, HttpStatus.OK);

		}
		catch(Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	
	@GetMapping("/employeepositionbyposition/{id}")
    public ResponseEntity<List<EmployeePosition>> GetAllEmployeePositionByPositionId(@PathVariable("id") Long id)
    {
        try
        {
            List<EmployeePosition> employeeposition = this.employeepositionRepo.FindByPositionId(id);
            return new ResponseEntity<>(employeeposition, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
	
	
	@PostMapping("/employeeposition")
	public ResponseEntity<Object> SaveEmployeePosition(@RequestBody EmployeePosition employeeposition){
		try {
			this.employeepositionRepo.save(employeeposition);
			return new ResponseEntity<>(employeeposition, HttpStatus.OK);
		}
		catch(Exception exception) {
			return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@GetMapping("/employeepositionmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<EmployeePosition> employeeposition = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<EmployeePosition> pageTuts;

            pageTuts = employeepositionRepo.findAll(pagingSort);

            employeeposition = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("employeeposition", employeeposition);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
	
	@GetMapping("/employeeposition/{id}")
	public ResponseEntity<List<EmployeePosition>> GetEmployeePositionById(@PathVariable("id") Long id) {
		try {
			Optional<EmployeePosition> employeeposition = this.employeepositionRepo.findById(id);
			if (employeeposition.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(employeeposition, HttpStatus.OK);
				return rest;
			}
			else {
				return ResponseEntity.notFound().build();
			}
		}
		catch (Exception exception) {
			return new ResponseEntity<>( HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("/employeeposition/{id}")
	public ResponseEntity<Object> UpdateEmployeePosition(@RequestBody EmployeePosition employeeposition, @PathVariable("id") Long id){
		Optional<EmployeePosition> employeepositionData = this.employeepositionRepo.findById(id);
		if (employeepositionData.isPresent()) {
			employeeposition.setId(id);
			this.employeepositionRepo.save(employeeposition);
			ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
			return rest;
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("/employeeposition/{id}")
	public ResponseEntity<Object> DeleteEmployeePosition(@PathVariable("id") Long id){
		Optional<EmployeePosition> employeepositionData = this.employeepositionRepo.findById(id);
		if(employeepositionData.isPresent()) {
			this.employeepositionRepo.deleteById(id);
			ResponseEntity rest = new ResponseEntity<>("Success",HttpStatus.OK);
			return rest;
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
