package com.example.EmployeePosition.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.EmployeePosition.Model.Position;
import com.example.EmployeePosition.Repository.PositionRepo;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiPositionController {
    @Autowired
	private PositionRepo positionRepo;
	
	@GetMapping("/position")
	public ResponseEntity<List<Position>> GetAllProduct(){
		try {
			List<Position> position = this.positionRepo.findAll();
			return new ResponseEntity<>(position, HttpStatus.OK);

		}
		catch(Exception exception) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
}
