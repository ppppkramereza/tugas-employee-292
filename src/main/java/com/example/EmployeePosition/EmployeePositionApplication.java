package com.example.EmployeePosition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeePositionApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeePositionApplication.class, args);
	}

}
