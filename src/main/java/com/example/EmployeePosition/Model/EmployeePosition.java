package com.example.EmployeePosition.Model;

import javax.persistence.*;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "employee_position")
@SQLDelete(sql = "UPDATE employee_position SET is_delete = true WHERE id = ?")
@Where(clause = "is_delete = false")
public class EmployeePosition {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long Id;
	
	@ManyToOne
	 @JoinColumn(name = "employee_id", insertable = false, updatable = false)
	 public Employee employee;

	 @Column(name = "employee_id", nullable = true)
	 private Long EmployeeId;
	 
	 @ManyToOne
	 @JoinColumn(name = "position_id", insertable = false, updatable = false)
	 public Position position;

	 @Column(name = "position_id", nullable = true)
	 private Long PositionId;

	 @Column(name = "is_delete", nullable = false)
	 private Boolean IsDelete = false;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Long getEmployeeId() {
		return EmployeeId;
	}

	public void setEmployeeId(Long employeeId) {
		EmployeeId = employeeId;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Long getPositionId() {
		return PositionId;
	}

	public void setPositionId(Long positionId) {
		PositionId = positionId;
	}

	public Boolean getIsDelete() {
		return IsDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		IsDelete = isDelete;
	}

	
	 
}
