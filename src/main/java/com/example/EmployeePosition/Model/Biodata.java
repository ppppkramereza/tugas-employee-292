package com.example.EmployeePosition.Model;

import javax.persistence.*;

@Entity
@Table(name = "biodata")
public class Biodata {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long Id;
	
	@Column(name = "first_name", nullable = false)
	private String FirstName;
	
	@Column(name = "last_name", nullable = false)
	private String LastName;
	
	@Column(name = "dob", nullable = true)
	private String Dob;
	
	@Column(name = "pob", nullable = true)
	private String Pob;
	
	@Column(name = "address", nullable = false)
	private String Address;
	
	@Column(name = "marital_status", nullable = false)
	private boolean MaritalStatus = false;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getDob() {
		return Dob;
	}

	public void setDob(String dob) {
		Dob = dob;
	}

	public String getPob() {
		return Pob;
	}

	public void setPob(String pob) {
		Pob = pob;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public boolean isMaritalStatus() {
		return MaritalStatus;
	}

	public void setMaritalStatus(boolean maritalStatus) {
		MaritalStatus = maritalStatus;
	}
	
	
	
}
