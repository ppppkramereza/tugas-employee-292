package com.example.EmployeePosition.Model;

import javax.persistence.*;

@Entity
@Table(name = "position")
public class Position {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private long Id;
	
	@Column(name = "name_position", nullable = false)
	private String NamePosition;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getNamePosition() {
		return NamePosition;
	}

	public void setNamePosition(String namePosition) {
		NamePosition = namePosition;
	}
	
	
}
